# ADIMLAR

silesia_tar.zip dosyası önce zip arşivinden çıkarılır, daha sonra da tar
arşivinden çıkarılır.

```
unzip silesia_tar.zip
tar xf silesia.tar --directory .
```

komutlar sırasıyla `time bash {isim}.sh` şeklinde çalıştırılır.

Örnek:
```
time bash tar.sh
```
